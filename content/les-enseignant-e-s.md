+++
title = "Les enseignants et enseignantes "
description = "Equipe pédagogique de la filière information-communication de l'Università di Corsica"
keywords = ["info-com", "licence info com", "master info com", "fllashs", "univ corse", "corte"]
+++

## SCIENCES DE L’INFO-COM

* ALBERTINI Françoise

* ANGELINI Julien

* CRISTOFARI Pasquin

* DOTTORI Lesia

* GRIMALDI Eliane

* KRUSLIN Noël

* MARCHETTI-LECA Pascal

* MEDORI Romain

* MORACCHINI Mélanie

* RICHARD-BATTESTI Romain

* ROMASCU Alina Elena

* SANTINI Dan Mathieu

* TALAMONI Jean-Guy

* VANUCCI Bernard

* VENTURINI Marie-Michèle

* VERDONI Dominique

 <br>

## SCIENCES HUMAINES ET SOCIALES

* ALBERTI Vanessa 

* ALBERTINI Alexandra

* COLLET Laurent

* FOGACCI Tony

* REY Didier

* TALAMONI Serena

​<br>

## INFORMATIQUE

* MANNU Patrick

* MARIANI Louis

* MORETTI Christophe

<br>

## LANGUES

* BALDACCI Claire

* BENEDETTI Davia 

* CASTELLI Marie-Pascale 

* FILIPPI Christophe

* LANDRON Fabien

* MC-MAHON Elizabeth

* RUGGERI Chrissie

* SORBA Nicolas

<br>