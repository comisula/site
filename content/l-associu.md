+++
title = "L'Associu Com'Isula"
description = "Association étudiante Com'Isula"
keywords = ["info-com","università di corsica","univ corse", "corte"]
+++

<div style="font-size: 1.5em;">
<p><strong>L’associu Com’Isula</strong> hè nata di i studienti in Scienze di l’Infurmazione è di a Cummunicazione di l’Università di Corsica in 2002. St’urganizazione hà per ambizione d’adunisce i studienti di a filiera d’Infurmazione è di Cummunicazione ma dinù di sviluppà a cummunicazione in Corsica (Furmazione di cullaborazione incu l’attore isulani di a Cummunicazione, urganizazione d’evvenimenti è manifestazione prufessiunale nantu l’infurmazione è a cummunicazione).</p>
<br/>
<p>In stu sensu, Com’Isula hà per ubbiettivu di sviluppà a filiera InfoCom è permette à tutti i so membri d’aiutassi trà di elli ! L’associu studientina Com’Isula porta valore cume a trasmissione, u scambiu è vole fà scopre a richezza è a diversità di a so filiera.</p>

</div>

<div style="text-align: center;"><img src="/img/comisula-associu.jpg" alt="Logo Com'Isula" /></div>