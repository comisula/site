+++
title = "La mise en valeur du patrimoine corse à l'université de Bucarest"
date = "2018-03-12T14:57:00+02:00"
categories = ["La vie en info-com"]
banner = "/img/blog/bucarest/bucarest-3.jpg"
showBanner = false
+++

Comment le patrimoine Corse a-t-il été mis en valeur à l’Université de Bucarest?

<!--more-->

Les étudiants de Master 1 Sciences de l’Information et de Communication de la FLLASHS, Università di Corsica ont eu le projet ambitieux d’organiser un voyage pédagogique à l’Université de Bucarest en Roumanie, afin de promouvoir le patrimoine culturel Corse. Ce projet s’est déroulé sous la direction de l’équipe enseignante suivante : Alina ROMASCU, Marie-Michèle VENTURINI, Christophe MORETTI et en étroite collaboration avec le coordinateur de la chaire UNESCO d’étude des échanges interculturels et inter-religieux de la Faculté de Philosophie, Université de Bucarest, Monsieur Lilian CIACHIR.

Pour récolter les fonds nécessaires à la réalisation du projet, les étudiants ont été amenés à organiser de multiples événements (tournoi de futsal, tombola, loto, marché de Noël…) et ont reçu plusieurs aides financières de l’Università di Corsica.

Durant ce voyage pédagogique qui s’est déroulé du 4 au 9 Mars 2018 ont été organisées deux conférences ainsi que des rencontres avec le Président de l’Université de Bucarest-PR Mircea DUMITRU, le Vice-président chargé des relations internationales-MCF Sorin COSTREIE, le doyen de la Faculté de Philosophie-PR Romulus BRANCOVEANU, le coordinateur de la chaire UNESCO d’étude des échanges interculturels et interreligieux, Monsieur Lilian CIACHIR, l’équipe des enseignants de la Faculté de Philosophie et des étudiants.

La première conférence qui s’est déroulée le 6 Mars à la Faculté de Philosophie de Bucarest (cf. photos n°1: Faculté de Philosophie de Bucarest), a essayé d’apporter des éléments de réponse à la question « Comment le patrimoine culturel Corse a-t-il été mis en valeur ? ». Durant cette conférence les étudiants ont abordé des sujets comme : le patrimoine immatériel (U cantu in Paghjella, U cantu di e donne et de Ricordi di Petru-Pà), le patrimoine matériel en présentant la mise en valeur de la châtaigne Corse qui est protégée par une Appellation d’Origine Contrôlée, ainsi que le patrimoine naturel et le patrimoine bâti en Corse, comme la réserve de Scandola (Patrimoine de l’UNESCO) ou encore les tours et ponts génois. Cette conférence s’est terminée par une réception afin de faire découvrir les spécialités culinaires de notre Île.

La deuxième conférence qui a eu lieu le 7 Mars à la Faculté de Droit de l’Université de Bucarest au sein de la salle Stoienescu (c.f. photos n°2: Salle du Rectorat de l'Université de Bucarest) s’est concentrée sur la « Présentation de la Corse et de l’Università di Corsica ». Elle a débuté par une présentation historique de la Corse et de l’Università di Corsica, poursuivie par une présentation de la filière Sciences de l’Information et de la Communication (Licence et Master) et qui s’est terminée par une présentation de la M3C (Médiathèque Culturelle de la Corse et des Corses).

Le même jour à partir de 18h30 les étudiants et l’équipe enseignante ont été conviées à une réception organisée par la Présidence de l’Université de Bucarest à « Casa Universitarilor » de Bucarest (photo n°) afin de fêter cette collaboration entre l’Università di Corsica et l’Université de Bucarest et de faire découvrir à la délégation Corse les spécialités culinaires Roumaines (photo n°3: Casa Universitarilor Bucuresti).

*Photo n°1 : Faculté de Philosophie de Bucarest*

![Faculté de philo](/img/blog/bucarest/bucarest-1.jpg)

*Photo n°2 : Salle du Rectorat de l’Université de Bucarest*

![Rectorat](/img/blog/bucarest/bucarest-2.jpg)

*Photo n°3 : Casa Universitarilor Bucuresti*

![Casa](/img/blog/bucarest/bucarest-3.jpg)