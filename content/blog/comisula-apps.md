+++
title = "Com'isula apps : une passerelle entre professionnels et  étudiants"
date = "2017-12-06T18:22:08+02:00"
categories = ["Les actualités des info-com"]
banner = "img/banners/comisulapps.jpg"
showBanner = true
+++

Les étudiants en deuxième année de Master en Sciences de l'Information et de la Communication ont dévoilé le 6 décembre 2017 leur application.

<!--more-->

Entrer dans la vie active est bien souvent le parcours du combattant pour un étudiant. Partant de ce constat, les étudiants ont eu l'idée ambitieuse de créer une application qui mettrait en contact étudiants en recherche d'expérience et entreprises professionnelles en recherche de compétence. Ce projet qui est pour l'heure qu'un prototype se veut être également un réseau collaboratif et d'entre-aide pour élaborer des projets de communication évènementielle. L'application serait disponible d'ici 2020 et pourrait s'étendre à l'ensemble des compétences et domaines pris en charge par l'Université de Corse.
