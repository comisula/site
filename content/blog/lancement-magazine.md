+++
title = "Lancement Com'Isula Magazinu"
date = "2017-03-29T18:11:00+02:00"
categories = ["Les actualités des info-com"]
banner = "img/banners/magazine.jpg"
showBanner = true
+++

Le 28 mars 2017 se déroulait le lancement du magazine Com’Isula sur le Campus Mariani. Projet réalisé par les étudiants de Licence 3 de Sciences de l’Information et de la Communication, le magazine se compose de 32 pages alliant articles informatifs, interviews de professionnels et rubriques humoristiques. L'événement s’est tenu à 15 heures, après une présentation informative du déroulé de leur projet les étudiants de licence ont offert un buffet aux invités, professeurs et étudiants présents.