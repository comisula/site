+++
title = "Une journée chez les info-com"
date = "2018-01-30T10:13:29+02:00"
categories = ["La vie en info-com"]
banner = "img/banners/journee.png"
showBanner = true
+++
<!--more-->

La journée typique d'un Info-Com commence à 9h avec un cours de graphisme pour apprendre à retoucher les images suivis d'un cours d’événementiel avec la présentation d'un événement d’après la méthode QQOQCP.

A midi : pause déjeuner au Restaurant Universitaire du Campus Mariani.

14h: retour en cours pour 2h sur l'histoire de la communication et sa théorie.

A 16h30, rassemblement des groupes de suivi pour avancer sur nos roets respectif (événementiel, site internet et magasine).

A 18h: fin de la journée !