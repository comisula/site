+++
title = "Nos objectifs"
date = "2018-01-30T10:20:59+02:00"
categories = ["La vie en info-com"]
banner = "img/equipe-new.jpg"
showBanner = true
+++
(Suite de "Nouvelle année, nouvelle équipe")

<!--more-->

Tout d’abord, nous allons commencer par la présentation de notre association Com’isula. Une rubrique « l’association » lui sera entièrement attribuée ! Vous pourrez en savoir un peu plus sur elle et ses actions. Véritable socle de la filière, elle représente l’identité commune qui permet à l’ensemble des étudiants de monter des projets de manière réelle. Une source non négligeable pour l’apprentissage des étudiants dans le domaine de l’information et de la communication.

Une rubrique spéciale sera créée afin de vous dévoiler les coulisses de nos études. La "filière" met à disposition des informations essentielles destinées à éveiller la curiosité des actuels et futurs étudiants. Cette rubrique est divisée en trois thèmes importants. "Enseignants et enseignantes", "études dans la filière", "les métiers de la com'", ont pour objectifs de vous donner un aperçu de ce que peut apporter et où peut amener ce domaine d’étude.

Les événements passés et à venir seront relayés dans la rubrique "actualité" tout comme les informations importantes à en retenir !

La rubrique « nous contacter » ressemble l’intégralité des coordonnées pour nous contacter en cas de pépin !

Et parce qu’il est important de remercier nos partenaires sans qui ce site n’aurait pas pu être en ligne, une rubrique « partenaires » dans laquelle vous pourrez les retrouver, leurs sera dédiés.