+++
title = "Nouvelle année, nouvelle équipe!"
date = "2018-01-30T10:14:58+02:00"
categories = ["La vie en info-com"]
banner = "img/equipe-new.jpg"
showBanner = true
+++
Bonjour à tous et à toutes et BIENVENUE !

Cette nouvelle année rime avec nouvelle équipe. Nous avons le plaisir de vous annoncer le changement d’équipe de gestion du site Internet.<!--more-->

Notre équipe de 11 étudiants en troisième année de licence information et communication se mobilise pour vous la présenter ! Vous découvrirez à travers chacune des rubriques, une dimension différente : le meilleur de l’actualité et des informations de la filière.