+++
title = "Questions-Réponses"
date = "2018-01-30T10:14:48+02:00"
categories = ["La vie en info-com"]
banner = "img/banners/questions.jpg"
showBanner = false
+++
À chaque question, sa réponse !

<!--more-->

### Faut-il avoir eu un baccalauréat spécifique pour intégrer cette licence ?

Non, l’accès à la licence se fait d’après les résultats obtenus au baccalauréat général ou technologique. L’admission est donc sur dossier. Par ailleurs un très bon niveau en français est recommandé.

### Comment sont organisées les 3 années de la licence ?

L’organisation du programme peut varier selon l’établissement. À l’université de Corse la licence information et communication est construite sur un nombre total d’heure égale à 1498.

Les années ont un volume d’heure progressives:

- 460 pour la première année

- 480 pour la seconde année

- 558 pour la dernière

### Que savoir sur la licence info-com ?

Elle est une formation pluridisciplinaire qui visent à enseigner les théories scientistes de bases, propres à comprendre les phénomènes constitutifs des sciences de l’information et de la communication. Des disciplines tel que la psychologie, la sociologie, la linguistique, l’anthropologie, le droit y sont enseignées pour consolider la culture générale de ce champs interdisciplinaires

La licence Information et Communication est une formation pluridisciplinaire déclinée en enseignements théoriques, en enseignements de spécialités professionnelles et à leur application dans le domaine des technologies.

Les objectifs de la formation visent à :

- Donner les bases d'une culture scientifique propre à comprendre les phénomènes de la communication.

- Enrichir les enseignements fondamentaux en Information et Communication par des disciplines comme la psychologie, la sociologie, l'anthropologie, la linguistique, le droit afin de permettre la constitution d'une solide culture générale.

- Former à la maitrise des enjeux contemporains des TIC.

- Donner aux étudiants les compétences pratiques par la réalisation de projets concrets permettant la mise en œuvre des savoirs et des outils.

- Former les étudiants à l'analyse des phénomènes de communication dans les entreprises, collectivités territoriales, institutions culturelles et à la maitrise des enjeux de l'information.

<br>

#### *N'hésitez pas à nous poser vos questions via la rubrique "Nous contacter". Nous vous y répondrons le plus rapidement possible !*