+++
title = "Les bonnes résolutions des Info-Com 2017-2018"
date = "2018-01-02T13:21:08+02:00"
categories = ["La vie en info-com"]
+++
Faire sa liste des bonnes résolutions à accomplir durant l'année est devenue une tradition ! Nous avons souhaité partager nos bonnes résolutions pour cette année 2018 !

<!--more-->

\#**Résolution 1** : Travailler encore et toujours plus, (au Bip's)

\#**Résolution 2** : Faire du sport (S.P.O.R.T = Séries.Pizzas.Oriente.Raclette.Télé)

\#**Résolution 3**: Manger bio, manger sainement (à part si on m'appelle à 19h pour aller manger au Coffee Cortenais)

\#**Résolution 4** : Ranger mon bordel (Mais avant, je fini la saison 1 de Casa De Papel...)

\#**Résolution 5**: Arrêter de se mettre une mine à l'Enjoy (parce qu'on a T.D demain...)

\#**Résolution 6** : Squatter plus à la B.U (mais c'est trop loin)

\#**Résolution 7**: Réviser mes cours après ma journée (au moins une fois par semestre)

\#**Résolution 8**: Faire attention à mes dépenses (j'attend toujours la bourse.....)

\#**Résolution 9** : Obtenir notre licence

\#**Résolution 10**: Et enfin, tenir toutes ses bonnes résolutions !