+++
title = "On vous remercie"
date = "2018-04-03T17:03:59+02:00"
categories = ["Les actualités des info-com"]
banner = "img/equipe-new.jpg"
showBanner = true
+++
C'est dans la gaieté et la bonne ambiance que s'est achevé le mardi 27 mars dernier, le projet des étudiants de la licence 3 information et communication.

Ils vous ont présenté tour à tour leur projet sur lequel ils ont travaillé durant toute l’année.

Vous avez été nombreux à assister à cet événement et espérons que cela vous a plu !

On partage avec vous la vidéo de présentation ainsi que le [MAGAZINU COM'ISULA](https://files.acrobat.com/a/preview/67a93dc5-e655-4aec-917e-9dc14c7e83ac) en cliquant sur ce lien !

<iframe width="854" height="480" src="https://www.youtube.com/embed/Ca46GxVtCyk" frameborder="0" allow="autoplay; encrypted-media" allowfullscreen></iframe>