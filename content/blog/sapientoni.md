+++
title = "Sapientoni Project"
date = "2017-03-30T20:34:00+02:00"
categories = ["Les actualités des info-com"]
banner = "img/banners/i-sapientoni.jpg"
showBanner = true
+++

Les étudiants du Master 2 Sciences de l’Information et de la Communication ont organisé un challenge "Sapientoni Project" le mercredi 7 décembre 2016 sur le campus Mariani à Corte.

Destiné à réunir et fédérer les étudiants de l'Università di Corsica autour d’un événement culturel aux valeurs identitaires fortes, ce challenge s’est organisé à travers une série d'épreuves : histoire, sport, musique, art et gastronomie.

La campagne d'inscription avait pour but de solliciter un maximum d’étudiants afin qu’ils composent une équipe de trois personnes (dont un étudiant corsophone). Une vingtaine d’équipes a été retenue pour participer au challenge le jour J.

À l’issue de cette compétition, les deux équipes finalistes ont pu s’affronter sur le plateau télévisé de « Sapientoni » le 9 décembre 2016 à Ajaccio.

![Sapientoni Project](/img/blog/sapientoni/sapientoni-2.jpg)

![Sapientoni Project](/img/blog/sapientoni/sapientoni-3.jpg)

![Sapientoni Project](/img/blog/sapientoni/sapientoni-4.jpg)

![Sapientoni Project](/img/blog/sapientoni/sapientoni-5.jpg)

![Sapientoni Project](/img/blog/sapientoni/sapientoni-6.jpg)

![Sapientoni Project](/img/blog/sapientoni/sapientoni-7.jpg)