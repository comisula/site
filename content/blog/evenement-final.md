+++
title = "Prochainement : événement final de la  présentation de la 2nd édition de  Com'Isula Magazinu et de Com'Isula Web"
date = "2018-03-24T10:20:59+02:00"
categories = ["Les actualités des info-com"]
banner = "img/banners/presentation-finale-thumb.jpg"
showBanner = true
+++
Le 27 Mars prochain à partir de 15h se déroulera en B1-204 la présentation de la 2nd édition du magazine et du site web Com'Isula par les étudiants de la licence 3 information et communication.

Un tirage de tombola, un photomaton, un buffet, et bien d'autres surprises viendront animer cette après-midi.