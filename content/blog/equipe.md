+++
title = "Une promotion, trois équipes, un projet"
date = "2018-01-24T10:13:59+02:00"
categories = ["La vie en info-com"]
banner = "img/banners/equipes.jpg"
showBanner = true
+++
La licence 3 information et communication de l’Université de Corse est une promotion de 40 étudiants issus de filières et d’horizons différents.

Pour cette année fatidique, les étudiants ont été scindés en 3 équipes afin de mettre en place les supports de communication de l’association Com’isula. Ils auront pour cela 8 mois.

<!--more-->

Au programme:

- Une équipe pour le magazine Com’isula : composé de 14 étudiants, l’équipe sera chargée du financement, de la rédaction et de la mise en page du Magazine. Ils ont fait appel à plusieurs sponsors afin de les soutenir !

- Une équipe événementielle : elle sera en charge de l'organisation intégrale des événements finaux des présentations des changements, et sera présente sur le parvis de l'université. Au programme, des stands ateliers en lien avec les différents articles du magazine et de notre site web.

- Une équipe chargée du Site internet Com’isula : Et le meilleur pour la fin, notre équipe, qui sera chargée de reprendre le site et de le mettre au goût du jour ! Pour cela, interviews, articles, vidéos seront prochainement disponible ! En attendant, tour d'horizon sur notre filière et sur ce qu'elle nous apporte.

Voilà, présentation faite, vous savez tout sur nos équipes et sur nous !

Pour cette troisème année, les étudiants se retrouvent autour des supports de communication de l’association Com’isula afin de les perfectionner et optimiser !

L'année universitaire 2017/2018 s'annonce chargée pour la licence 3 d'info&com !