+++
title = "Un tournoi de Rami à la hauteur de nos espérances"
date = "2017-11-13T13:58:46+02:00"
categories = ["Les actualités des info-com"]
banner = "img/banners/rami.jpg"
showBanner = true
+++

Le lundi 13 novembre 2017 les étudiants ont organisés un tournoi de Rami au Café de la Place afin de récolter les fonds nécessaire pour souscrire l'abonnement du site internet ! La soirée a été à la hauteur de leurs espérances.

L’événement leur a permis d'activer le Site Internet et d'entreprendre les actions qu'ils s'étaient fixés au préalable! Désormais ils vont pouvoir commencer leur projet !
