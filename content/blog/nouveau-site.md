+++
title = "Bienvenue sur le nouveau site!"
date = "2018-10-19T16:09:12+02:00"
categories = ["Les actualités des info-com"]
banner = "img/comisula-associu.jpg"
showBanner = true
+++
C'est avec un grand plaisir que nous vous présentons le nouveau design du site web de l'Associu Com'Isula!

<!--more-->

De nombreuses améliorations sont désormais disponibles, telles que [l'affichage adaptatif](https://fr.wikipedia.org/wiki/Site_web_adaptatif) pour un comfort de lecture optimal peu importe la taille de votre appareil, un chargement beaucoup plus rapide grâce à la génération de pages web statiques, et un [formulaire de contact](/contact) par e-mail.

De plus, le site passe désormais à un hébergement sous OVH, permettant de réduire le coup de près d'1/6.

La plupart des anciennes actualités de l'association et de la filière sont disponibles sur le nouveau site, mais l'ancien est toujours accessible à [cette adresse](https://associucomisula.wixsite.com/website-5). Si ce lien ne fonctionne plus, merci de nous contacter.