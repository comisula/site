+++
title = "Les métiers de la com"
description = "Aperçu des métiers de l'information et de la communication"
keywords = ["info-com", "licence info com", "master info com", "formation info com"]
+++

<p style="font-size: 1.3em">Le secteur de la communication rassemble un large éventail d'activités, visant à promouvoir l'image d'un produit, d'un service, d'un événement, d'une personne ou d'une organisation.</p>

## Dans les organisations

* #### Directeur de la com'

    - Il choisit une stratégie de communication (interne ou externe) et veille à sa mise en oeuvre. Il a sous ses ordres des chargés de com.

* #### Community manager

    - Chargé d'assurer la présence d'une marque ou d'une entreprise sur les réseaux en fédérant autour d'elle une communauté d'internautes.

* #### Attaché de presse

    - C'est le lien entre les entreprises et les médias, le porte parole de la campagne de communication. Promeut l'activité auprès des journalistes.

* #### Chargé de com'

    - Métier polyvalent: réalisation supports, élaboration plan médias, relations presse, gestion web, événementiel...

* #### Chargé des relations publiques

    - Vérifie, sélectionne des infos, développe une stratégie, se spécialise dans un domaine.

* #### Journaliste d'entreprise

    - Très répandu dans la communication interne, valorise les activités des différents services.

* #### Concepteur-rédacteur

    - Il trouve des idées et des mots qui transformeront un slogan publicitaires en une formule choc

* #### Web-designer

    - Conçoit l'architecture des sites internet ou projets digitaux.

## En agence de communication

* #### Directeur artistique

    - Élabore un projet visuel pour illustrer la campagne (presse, radio, TV, affichage).

* #### Média-planner

    - Le choix stratégique des meilleurs médias à destination du meilleur impact sur la cible et son essor.

* #### Acheteur d'espace publicitaire

    - Exécute la commande du média-planner. Il contacte les fournisseurs et engage la fabrication des supports.

* #### Responsable événementiel

    - Organise des opérations de communication

* #### Rédacteur

    - Il commence par la collecte de l'information puis rédige son article, en tenant compte de la ligne éditoriale et du support.

* #### Secrétaire de rédaction

    - Corrige les articles, leurs mises en formes, en veillant à la cohérence de la publication

​
## Dans les médias

* #### Rédacteur en chef

    - Réfléchit à la mise en page de la publication en organisant et en positionnant des articles et des illustrations. Il donne son aval sur le contenu rédactionnel et visuel.

<br>