### Les indispensables:

ARISTOTE. Rhétorique, Paris, édition Flammarion, 2007
BARBUSSE Beatrice- Du sexisme dans le sport Paris : édition Anamosa, 2016. 264 pages.
BENILDE Marie, On achète bien les cerveaux: la publicité et les médias. Paris, édition Raisons d'agir, 2007. 160 pages.
BOUGNOUX, Daniel. Introduction aux sciences de la communication, Paris, édition La Découverte, 2001. 125 pages.
BOURDIEU, Pierre. Sur la télévision : suivi de l'Emprise du joumalisme, Paris, édition Raisons D'Agir, 2008. 95 pages.
BRETON Philippe. Convaincre sans manipuler: apprendre à argumenter, Paris, édition La Découverte, 2015. 119 pages.
BRETON Philippe. La parole manipulée, Paris, édition Découverte, 1997. 220 pages.
BRETON Philippe. GAUTHIER Gilles, Histoire des théories de l'argumentation, Paris, édition La Découverte, 2012. 128 pages.
BRETON Philippe, L'utopie de la communication : le mythe du «village planétaire», Paris, édition La Découverte, 1997. 171 pages.
DE BAYNAST Arnaud, LENDREVIE Jacques, LEVY Julien, Mercator-12ème éd. : Tout le marketing à l'ère digitale. Volume 1 de Marketing master, Livres en Or. Paris: édition Dunod, 6 sept. 2017. 1040 pages.
FOUCAULT Michel - Surveiller et punir. Naissance de la prison. Paris, édition Gallimard, 2014. 360 pages.
KAUFMANN Jean-Claude — Corps de femme, regard d'homme, sociologie des seins nus sur la plage. Paris : Édition Pocket, 2001. 294 pages.
LEBON Gustave. L'opinion et la foule. Paris : édition FV Éditions, 8 sept. 2013.
MACE Éric, La société et son double : une journée ordinaire à la télévision, Paris: édition Armand Colin, 2006. 318 pages.
MUCCHIELLI Alex, Introduction aux SIC. 4ème édition. Paris: Hachette Éducation, 28 juin 2006. 160 pages.
TARDE Gabriel — Les lois de l'imitation. Paris : édition Ink book, 1 avr. 2013.
TCHAKOTINE Serge, Le viol des foules par la propagande politique, Paris, édition Gallimard, 1992.
WATZLAWICK Paul, HELMICK BEAVIN Janet, JACKSON Don D. Une logique de communication, Paris, édition du Seuil, 2014. 280 pages.
WIENER Norbert, LE ROUX Ronan. Cybernétique et société. L'usage humain des êtres. Paris, édition du Seuil, 2014. 220 pages.
WINKIN Yves. Anthropologie de la communication: de la théorie au terrain, Paris, édition Boeck Université, 2001. 332 pages.

### Pour les plus téméraires:

ADAMS INNIS, Harold, l'oiseau de Minerve, Paris, édition Persée, 1983. 296 pages.
BECKER Howard - Outsiders : étude de la sociologie de la déviance, Paris, éditions Métailié, 1985. 250 pages.
BOUGNOUX Daniel - Introduction aux sciences de la communication, Paris, édition La découverte, 2002. 128 pages.
Daniel BOUGNOUX. Sciences de l'information  et de la communication, Paris, édition Larousse, 1993. 809 pages.
DEBORD Guy, La société du spectacle, Paris, édition Gallimard, 1996. 224 pages.
Edward T. HALL. la dimension cachée, Paris, édition Essais, 2014. 256 pages.
FERREOL Gilles, NORECK Jean-Pierre - Introduction à la sociologie, Paris, édition Armand Colin - Collection cursus, 2010. 256 pages.
FERREOL Gilles (dir.) - Sociologie, Paris, édition Bréal, 2004. 416 pages.
HALEY Jay, Un thérapeute hors du commun : Milton H. Erickson, Paris, édition Desclée De Brouwer. 384 pages.
HOGGART Richard, La culture du pauvre, Paris, édition de minuit, 1970. 424 pages.
WINKIN Yves (dir.) BATESON Gregory, Jay BIRDWHISTELL, Don JACKSON et al. la nouvelle communication, Paris, édition Essais, 2014. 400 pages.

### Pour les passionnés:

Arnauld, DU MOULIN DE LABARTHETE - La communication événementielle: réussir conférences, colloques, conventions. Paris, édition Demos, 2010. 151 pages.
BALLE Francis. 2001. Médias et sociétés : Édition, réseaux sociaux, big data, presse, cinéma, GAFA, divertissement, information, radio, Web, télévision, communication, droit à l'oubli, Internet, audimat, VOD. Paris, Édition LGDJ, 2016. 892 pages.
BARTHES Roland. Le degré zéro de l'écriture, Paris, édition Pierres vives, 1953. 128 pages.
BOURDIEU Pierre — Propos sur le champ politique. Paris, édition Presses universitaires Lyon, 2000. 110 pages.
BOURDIEU Pierre — Sur la télévision. Paris, édition Raisons D'Agir, 2008. 95 pages.
DURKHEIM Emile. Les règles de la méthode sociologique, Paris, édition Flammarion, 2009.
GOUREVITCH, Jean-Paul. L'image en politique : de Luther à Internet et de l'affiche au clip, Paris, Hachette Littératures, 1998. 247 pages.
LEVI-STRAUSS Claude. L'identité: séminaire interdisciplinaire, Paris, édition Presses universitaires de France, 2007. 344 pages.
Mark, TUNGATE, Le monde de la pub: Histoire globale (et inédite) de la publicité. Paris, édition Dunod, 2009. 288 pages.
MAUSS Marcel. Essai sur le don. Paris, édition République des Lettres, 2013. 196 pages.
PASCAL Christophe et TURCO Olivier - Just Wahou !: Welcome dans le monde déjanté de l'évènementiel. Édition Bréal, 2011. 111 pages.
TYLOR Edward Burnett. La civilisation primitive, Volume 1, Paris, édition General Books, 2012. 78 pages.
VERNETTE Eric. L'essentiel du marketing : marketing fondamental. Paris, édition d'Organisation, 1998. 382 pages.
VON BERTALANFFY Ludwig. Théorie générale des systèmes, Paris, édition Dunod, 2002.
WATZLAWICK Paul, BANSARD Denis. Le langage du changement : éléments de communication thérapeutique. Paris, édition Seuil, 1986. 184 pages.
WATZLAWICK, Paul, La réalité de la réalité : confusion, désinformation, communication. Paris, édition Points, 2014. 237 pages.

### Pour aller plus loin...

ARENDT, Hannah, La crise de la culture : huit exercices de pensée politique, Paris, édition Gallimard, 1989. 380 pages.
ARENDT, Hannah, Paule, RICOEUR. Condition de l'homme moderne, Paris, édition Calmann-Lévy, 1988. 406 pages.
CAUNE, Jean, Culture et communication : convergences théoriques et lieux de médiation, édition des Presses Universitaires de Grenoble (PUG), 1995. 135 pages.
DE CERTEAU, Michel - L'invention du quotidien, volume 7. Paris, édition Union Générale d'éditions, 1980. 374 pages.
GADAMER, Hans-Georg, Vérité et méthode: les grandes lignes d'une herméneutique philosophique, Paris, édition Seuil 1976. 346 pages.
HABERMAS, Jürgen, L'espace public : archéologie de la publicité comme dimension constitutive de la société bourgeoise. Edition Payot, 1988. 324 pages.
JAUSS, Hans Robert, Pour une esthétique de la réception, Paris, édition Gallimard, 1990. 333 pages.
LAMIZET, Bernard, la médiation culturelle, édition l'Harmattan, 2000. 448 pages.
Mattelart, Armand et Michel, Histoire des théories de la communication, édition La Découverte, 2004. 123 pages.
MCLUHAN Marshall - Pour comprendre les médias: les prolongements technologiques de l'homme, éditions HMH, 1968. 404 pages.
MCLUHAN Marshall, La galaxie Gutenberg: la genèse de l'homme typographique, CNRS éditions, 2017. 544 pages.
STEINER, George - Réelles présences. les arts du sens, Paris, édition Gallimard,1994. 280 pages.

